-- -----------------------------  quan li sinh vien day01 ----------------------
create database if not exists QLSV;
use QLSV;


CREATE TABLE DMKHOA(
	MaKH VARCHAR(6) PRIMARY KEY,
	TenKhoa VARCHAR(30)
);

CREATE TABLE SINHVIEN(
	MaSV VARCHAR(6) PRIMARY KEY,
	HoSV VARCHAR(30),
    TenSV VARCHAR(15),
    GioiTinh CHAR(1),
    NgaySinh datetime ,
    NoiSinh VARCHAR(50),
    DiaChi VARCHAR(50),
    MaKH VARCHAR(6),
    HocBong int
);